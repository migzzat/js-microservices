 const bodyParser = require('body-parser')
 const path = require('path')

 const port = 3003
 const express = require('express')
 const app = express()


 app.use(bodyParser.json())
 app.use(bodyParser.urlencoded({
     extended: true
 }))

 app.get('/', (req, res) => {
     res.json({
         code: 1001,
         endpoint: 'users'
     })
 })


 app.listen(port, () => {
     console.log(`~user node~ running on port [::${port}]`)
 })